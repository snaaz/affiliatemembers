<?php
namespace Shake\Affiliate\Controller\Adminhtml\Gridcontroller;

use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\View\Result\PageFactory;


class Index extends Action
{
    
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $resultPageFactory;

    /**
     * @var \Magento\Backend\Model\View\Result\Page
     */
    protected $resultPage;
    protected $_statusField     = 'is_active';
    /**
     * @param Context $context
     * @param PageFactory $resultPageFactory
     */
    public function __construct(
            
        Context $context,
        PageFactory $resultPageFactory
    )
    {
        	
        parent::__construct($context);
        $this->resultPageFactory = $resultPageFactory;
    }

    public function execute()
    {
		
		$this->resultPage = $this->resultPageFactory->create();  
		$this->resultPage->setActiveMenu('Shake_Gridcontroller::members');
		$this->resultPage ->getConfig()->getTitle()->set((__('Memberssss')));
               
		return $this->resultPage;
    }
}
