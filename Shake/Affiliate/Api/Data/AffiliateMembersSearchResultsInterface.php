<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Shake\Affiliate\Api\Data;

/**
 * Interface for customer search results.
 */
interface AffiliateMembersSearchResultsInterface extends \Magento\Framework\Api\SearchResultsInterface
{
    /**
     * Get customers list.
     *
     * @api
     */
    public function getItems();

    /**
     * Set customers list.
     *
     * @api
     * @return $this
     */
    public function setItems(array $items);
}
