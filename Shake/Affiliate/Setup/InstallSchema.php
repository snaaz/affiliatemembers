<?php

/**
 * Copyright © 2015 Ihor Vansach (ihor@Shake.com). All rights reserved.
 * See LICENSE.txt for license details (http://opensource.org/licenses/osl-3.0.php).
 *
 * Glory to Ukraine! Glory to the heroes!
 */

namespace Shake\Affiliate\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\DB\Adapter\AdapterInterface;

/**
 * Members setup
 */
class InstallSchema implements InstallSchemaInterface {

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context) {
        $installer = $setup;

        $installer->startSetup();

        /**
         * Create table 'magefan_blog_post'
         */
        $table = $installer->getConnection()->newTable(
                        $installer->getTable('affiliate_members')
                )->addColumn(
                        'id', \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER, null, ['identity' => true, 'nullable' => false, 'primary' => true], 'ID'
                )->addColumn(
                        'name', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true], 'Name'
                )->addColumn(
                        'image', \Magento\Framework\DB\Ddl\Table::TYPE_TEXT, 255, ['nullable' => true], 'Image'
                )->addColumn(
                        'status', \Magento\Framework\DB\Ddl\Table::TYPE_SMALLINT, null, ['nullable' => false, 'default' => '1'], 'Status'
                )->addColumn(
                        'created', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, [], 'Created'
                )->addColumn(
                'modified', \Magento\Framework\DB\Ddl\Table::TYPE_TIMESTAMP, null, [], 'Modified'
        );
        $installer->getConnection()->createTable($table);
        $installer->endSetup();
    }

}
