<?php

namespace Shake\Affiliate\Model;

use Shake\Affiliate\Api\AffiliateMembers;
use Shake\Affiliate\Model\ResourceModel\Members\CollectionFactory;
use Magento\Framework\Api\SearchCriteriaInterface;

class AffiliateMembersModel implements AffiliateMembers {

    /**
     * @var JoinProcessorInterface
     */
    const STATUS_ENABLED = 'enabled';
    
    const STATUS_DISABLED = 'disabled';

    protected $searchResultsFactory;
    
    protected $memebersFactory;

    public function __construct(
    \Shake\Affiliate\Model\ResourceModel\Members\CollectionFactory $membersFactory,
    \Shake\Affiliate\Api\Data\AffiliateMembersSearchResultsInterfaceFactory $searchResultsFactory
    ) {
        $this->memebersFactory = $membersFactory;
        $this->searchResultsFactory = $searchResultsFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function affiliateMembers(\Magento\Framework\Api\SearchCriteria $searchCriteria) {

        $searchResults = $this->searchResultsFactory->create();
        
        $searchResults->setSearchCriteria($searchCriteria);
        
        $collection = $this->memebersFactory->create();

        foreach ($searchCriteria->getFilterGroups() as $group) {

            foreach ($group->getFilters() as $filter) {

                if ($filter) {
                    
                    $status = $this->getAvailableStatuses($filter->getValue());
                    
                    $collection->addFieldToFilter($filter->getField(), $status);
                }
            }
            return $collection->getData();
        }
    }

    public function getAvailableStatuses($status) {
        
        if ($status = self::STATUS_ENABLED) {
            
            return $status = '1';
            
        } else {
            
            return $status = '0';
        }
    }

}
