<?php
/**
 * Copyright © 2015 Shake. All rights reserved.
 */
namespace Shake\Affiliate\Model\ResourceModel;

/**
 * ModelName resource
 */
class Members extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    /**
     * Initialize resource
     *
     * @return void
     */
    public function _construct()
    {
        
        $this->_init('affiliate_members', 'id');
    }
  public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        \Magento\Framework\Stdlib\DateTime\DateTime $date,
        \Magento\Framework\Stdlib\DateTime $dateTime,
        $resourcePrefix = null
    ) {
        parent::__construct($context, $resourcePrefix);
        $this->_date = $date;
        $this->dateTime = $dateTime;    
        $this->_init('affiliate_members', 'id');
    }
   protected function _beforeSave(\Magento\Framework\Model\AbstractModel $object)
    {
     
        $gmtDate = $this->_date->gmtDate();

        if ($object->isObjectNew() && !$object->getCreated()) {
            $object->setCreated($gmtDate);
        }

        $object->setModified($gmtDate);

        return parent::_beforeSave($object);
    }

}
