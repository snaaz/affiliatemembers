<?php

/**
 * Copyright © 2015 YourCompanyName. All rights reserved.
 */

namespace Shake\Affiliate\Model;

use Magento\Framework\Exception\ModelNameException;

/**
 * ModelNametab modelname model
 */
class Members extends \Magento\Framework\Model\AbstractModel {

    const STATUS_ENABLED = 1;
    const STATUS_DISABLED = 0;

    /**
     * Base media folder path
     */
    const BASE_MEDIA_PATH = 'shake_affiliate';

    /**
     * Prefix of model events names
     *
     * @var string
     */
    protected $_eventPrefix = 'shake_affiliate_members';

    public function isActive() {
        return ($this->getStatus() == self::STATUS_ENABLED);
    }

    public function getImage() {
        if (!$this->hasData('image')) {
            if ($file = $this->getData('image')) {
                $image = $this->_url->getMediaUrl($file);
            } else {
                $image = false;
            }
            $this->setData('image', $image);
        }

        return $this->getData('image');
    }

    /**
     * Retrieve available post statuses
     * @return array
     */
    public function getAvailableStatuses() {
        return [self::STATUS_DISABLED => __('Disabled'), self::STATUS_ENABLED => __('Enabled')];
    }

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\Db $resourceCollection
     * @param array $data
     */
    public function __construct(
    \Magento\Framework\Model\Context $context, \Magento\Framework\Registry $registry, \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null, \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null, array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
    }

    /**
     * @return void
     */
    public function _construct() {

        $this->_init('Shake\Affiliate\Model\ResourceModel\Members');
    }

}
