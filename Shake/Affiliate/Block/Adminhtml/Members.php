<?php
namespace Shake\Affiliate\Block\Adminhtml;
class Members extends \Magento\Backend\Block\Widget\Grid\Container
{
    /**]
     * Constructor
     *
     * @return void
     */
    protected function _construct()
    {
	
        $this->_controller = 'adminhtml_members';/*block grid.php directory*/
        $this->_blockGroup = 'Shake_Affiliate';
        $this->_headerText = __('Members');
        $this->_addButtonLabel = __('Add New Entry'); 
        parent::_construct();
		
    }
}